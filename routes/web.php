<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\DriverController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Authenticated routes
Route::middleware('web')->group(function () {


    // تسجيل الدخول لمسءول النظام
    Route::get('/', [UserController::class, 'showLoginForm'])->name('showLoginForm');
    Route::get('admin/login', [UserController::class, 'showLoginForm'])->name('showLoginForm');
    Route::post('admin/login', [UserController::class, 'login'])->name('doLogin');
    //    تسجيل الخروج لمسءول النظام
    Route::get('/logout', [UserController::class, 'logout'])->name('logout');

    // Dashboard route (protected by auth middleware)
    Route::middleware(['auth'])->group(function () {
        // لوحة التحكم
        Route::prefix('admin')->group(function () {
            Route::get('/dashboard', [HomeController::class, 'home'])->name('homeDashboard');
            Route::resource('users', ClientController::class);
            Route::resource('drivers', DriverController::class);
            Route::resource('orders', OrderController::class);
        });
    });

});
