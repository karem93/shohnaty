@extends('admin.layouts.app')
<style>
    .checkbox-container {
        display: grid;
        grid-template-columns: repeat(4, 1fr); /* Four checkboxes per row */
        gap: 5px; /* Adjust as needed */
    }

    .checkbox-container input[type="checkbox"] {
        display: none; /* Hide the default checkboxes */
    }

    .checkbox-container label {
        display: block;
        background-color: #f0f0f0; /* Optional background color */
        padding: 5px 10px;
        cursor: pointer;
    }

    .checkbox-container input[type="checkbox"]:checked + label {
        background-color: #007BFF; /* Change background color when checkbox is checked */
    }
</style>
@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">الرئيسية - تعديل بيانات العميل</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">الرئيسية</a></li>
                        <li class="breadcrumb-item active">تعديل بيانات العميل</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('drivers.update', $driver->id) }}" method="post"
                          enctype="multipart/form-data" class="mx-auto">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <!-- Name -->
                            <div class="form-group">
                                <label for="exampleInputText1">الاسم</label>
                                <input type="text" class="form-control" name="name" id="exampleInputText1"
                                       placeholder="ادخل الاسم" value="{{ $driver->name }}">
                            </div>


                            <!-- Password -->
                            <div class="form-group">
                                <label for="exampleInputPassword1">كلمة المرور</label>
                                <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                                       placeholder="ادخل كلمة المرور">
                            </div>

                            <!-- Phone Number -->
                            <div class="form-group">
                                <label for="exampleInputEmail1">الرقم القومي</label>
                                <input type="text" class="form-control" name="identity_number" id="exampleInputText1"
                                       placeholder="ادخل الرقم القومي" value="{{ $driver->identity_number }}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">رقم الهاتف</label>
                                <input type="text" class="form-control" name="phone" id="exampleInputText1"
                                       placeholder="ادخل رقم الهاتف" value="{{ $driver->phone }}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">الصوره</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                        <label class="custom-file-label" for="exampleInputFile"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <br><br>
                                    </div>
                                </div>
                            </div>

                            <h4>بيانات السيارة</h4>

                            <div class="form-group">
                                <label for="plate_number">رقم اللوحة</label>
                                <input type="text" required name="plate_number" value="{{ $driver->car?->plate_number }}"
                                       class="form-control"/>
                            </div>
                            <div class="form-group">
                                <label for="plate_number">نوع السيارة</label>
                                <select name="car_type" class="form-control">
                                    <option selected disabled>اختر نوع السيارة</option>
                                    <option value="ربع نقل" @if($driver->car?->car_type == 'ربع نقل') selected @endif>ربع نقل</option>
                                    <option value="جامبو" @if($driver->car?->car_type == 'جامبو') selected @endif>جامبو</option>
                                    <option value="نقل كبير" @if($driver->car?->car_type == 'نقل كبير') selected @endif>نقل كبير</option>
                                    <option value="سوزوكي" @if($driver->car?->car_type == 'سوزوكي') selected @endif>سوزوكي</option>
                                    <option value="مقطورة كبيرة" @if($driver->car?->car_type == 'مقطورة كبيرة') selected @endif>مقطورة كبيرة</option>
                                    <option value="قلاب" @if($driver->car?->car_type == 'قلاب') selected @endif>قلاب</option>
                                    <option value="جرار" @if($driver->car?->car_type == 'جرار') selected @endif>جرار</option>
                                    <option value="مازدا" @if($driver->car?->car_type == 'مازدا') selected @endif>مازدا</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="accessories">الكماليات</label>
                                <div class="checkbox-container">
                                    <input type="checkbox" id="accessory1" name="accessories[]" value="بوم"  @if(isset($accessories) && in_array('بوم',$accessories)) checked @endif>
                                    <label for="accessory1">بوم</label>

                                    <input type="checkbox" id="accessory2" name="accessories[]" value="كساتن" @if(isset($accessories) && in_array('كساتن',$accessories)) checked @endif>
                                    <label for="accessory2">كساتن</label>

                                    <input type="checkbox" id="accessory3" name="accessories[]" value="شرايط" @if(isset($accessories) && in_array('شرايط',$accessories)) checked @endif>
                                    <label for="accessory3">شرايط</label>

                                    <input type="checkbox" id="accessory4" name="accessories[]" value="جنازير" @if(isset($accessories) && in_array('جنازير',$accessories)) checked @endif>
                                    <label for="accessory4">جنازير</label>

                                    <input type="checkbox" id="accessory5" name="accessories[]" value="مشمع" @if(isset($accessories) && in_array('مشمع',$accessories)) checked @endif>
                                    <label for="accessory5">مشمع</label>

                                    <input type="checkbox" id="accessory6" name="accessories[]" value="زوي" @if(isset($accessories) && in_array('زوي',$accessories)) checked @endif>
                                    <label for="accessory6">زوي</label>

                                    <input type="checkbox" id="accessory7" name="accessories[]" value="سلب" @if(isset($accessories) && in_array('سلب',$accessories)) checked @endif>
                                    <label for="accessory7">سلب</label>

                                    <input type="checkbox" id="accessory8" name="accessories[]" value="سيلات" @if(isset($accessories) && in_array('سيلات',$accessories)) checked @endif>
                                    <label for="accessory8">سيلات</label>

                                    <input type="checkbox" id="accessory9" name="accessories[]" value="كاميرا" @if(isset($accessories) && in_array('كاميرا',$accessories)) checked @endif>
                                    <label for="accessory9">كاميرا</label>

                                    <input type="checkbox" id="accessory10" name="accessories[]" value="انذار رجوع للخلف" @if(isset($accessories) && in_array('انذار رجوع للخلف',$accessories)) checked @endif>
                                    <label for="accessory10">انذار رجوع للخلف</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">رخصة السيارة </label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile"
                                               name="car_license">
                                        <label class="custom-file-label" for="exampleInputFile"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <br><br>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputFile">رخصة القيادة </label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile"
                                               name="driving_license">
                                        <label class="custom-file-label" for="exampleInputFile"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <br><br>
                                    </div>
                                </div>
                            </div>


                            <div>
                                <button type="submit" class="btn btn-primary">تعديل</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
