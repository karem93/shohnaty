@extends('admin.layouts.app')


@section('content')
    <div class="content">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark"> الرئيسية - تفاصيل المرضى</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/dashboard">الرئيسية</a></li>
                            <li class="breadcrumb-item active">تفاصيل المرضى</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <img src="{{ $patient->image ? asset('uploads/' . $patient->image) : asset('images/no-image.png') }}" class="card-img-top img-fluid mx-auto d-block w-25" alt="{{ $patient->user->name }}">
                        <div class="card-body text-center">
                            <h2 class="text-center">{{ $patient->user->name }}</h2>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item"><strong>العمر</strong><br>{{ $patient->age }}</li>
                            <li class="list-group-item">
                                <strong>البريد الإلكتروني</strong><br>
                                {{ $patient->user->email }}
                            </li>
                            <li class="list-group-item">
                                <strong>رقم الهاتف</strong><br>
                                {{ $patient->user->phone }}
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
