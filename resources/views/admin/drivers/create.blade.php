@extends('admin.layouts.app')
<style>
    .checkbox-container {
        display: grid;
        grid-template-columns: repeat(4, 1fr); /* Four checkboxes per row */
        gap: 5px; /* Adjust as needed */
    }

    .checkbox-container input[type="checkbox"] {
        display: none; /* Hide the default checkboxes */
    }

    .checkbox-container label {
        display: block;
        background-color: #f0f0f0; /* Optional background color */
        padding: 5px 10px;
        cursor: pointer;
    }

    .checkbox-container input[type="checkbox"]:checked + label {
        background-color: #007BFF; /* Change background color when checkbox is checked */
    }
</style>

@section('content')
    <!-- /.card-header -->

    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">الرئيسية - اضافة السائقين</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">الرئيسية</a></li>
                        <li class="breadcrumb-item active"> اضافة السائقين</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <!-- form start -->
                <form role="form" method='post' action="{{ route('drivers.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger text-center">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- Name -->
                        <div class="form-group">
                            <label for="name">الاسم</label>
                            <input type="text" required name="name" value="{{ old('name') }}" class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">الصورة </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                    <label class="custom-file-label" for="exampleInputFile"></label>
                                </div>
                                <div class="input-group-append">
                                    <br><br>
                                </div>
                            </div>
                        </div>
                        <!-- Phone Number -->
                        <div class="form-group">
                            <label for="phone">رقم الهاتف</label>
                            <input type="text" required name="phone" value="{{ old('phone') }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="identity_number">الرقم القومي</label>
                            <input type="text" required name="identity_number" value="{{ old('identity_number') }}"
                                   class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label for="password">كلمة المرور</label>
                            <input type="password" required name="password" class="form-control"/>
                        </div>

                        <h4>بيانات السيارة</h4>

                        <div class="form-group">
                            <label for="plate_number">رقم اللوحة</label>
                            <input type="text" required name="plate_number" value="{{ old('plate_number') }}"
                                   class="form-control"/>
                        </div>
                        <div class="form-group">
                            <label for="plate_number">نوع السيارة</label>
                            <select name="car_type" class="form-control">
                                <option selected disabled>اختر نوع السيارة</option>
                                <option value="ربع نقل">ربع نقل</option>
                                <option value="جامبو">جامبو</option>
                                <option value="">نقل كبير</option>
                                <option value="سوزوكي">سوزوكي</option>
                                <option value="مقطورة كبيرة">مقطورة كبيرة</option>
                                <option value="قلاب">قلاب</option>
                                <option value="جرار">جرار</option>
                                <option value="مازدا">مازدا</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="accessories">الكماليات</label>
                            <div class="checkbox-container">
                                <input type="checkbox" id="accessory1" name="accessories[]" value="بوم">
                                <label for="accessory1">بوم</label>

                                <input type="checkbox" id="accessory2" name="accessories[]" value="كساتن">
                                <label for="accessory2">كساتن</label>

                                <input type="checkbox" id="accessory3" name="accessories[]" value="شرايط">
                                <label for="accessory3">شرايط</label>

                                <input type="checkbox" id="accessory4" name="accessories[]" value="جنازير">
                                <label for="accessory4">جنازير</label>

                                <input type="checkbox" id="accessory5" name="accessories[]" value="مشمع">
                                <label for="accessory5">مشمع</label>

                                <input type="checkbox" id="accessory6" name="accessories[]" value="زوي">
                                <label for="accessory6">زوي</label>

                                <input type="checkbox" id="accessory7" name="accessories[]" value="سلب">
                                <label for="accessory7">سلب</label>

                                <input type="checkbox" id="accessory8" name="accessories[]" value="سيلات">
                                <label for="accessory8">سيلات</label>

                                <input type="checkbox" id="accessory9" name="accessories[]" value="كاميرا">
                                <label for="accessory9">كاميرا</label>

                                <input type="checkbox" id="accessory10" name="accessories[]" value="انذار رجوع للخلف">
                                <label for="accessory10">انذار رجوع للخلف</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputFile">رخصة السيارة </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile"
                                           name="car_license">
                                    <label class="custom-file-label" for="exampleInputFile"></label>
                                </div>
                                <div class="input-group-append">
                                    <br><br>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputFile">رخصة القيادة </label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="exampleInputFile"
                                           name="driving_license">
                                    <label class="custom-file-label" for="exampleInputFile"></label>
                                </div>
                                <div class="input-group-append">
                                    <br><br>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="user_type" value="driver">
                        <div>
                            <button type="submit" class="btn btn-primary">تأكيد</button>
                        </div>
                    </div>

                    <!-- /.card-body -->
                </form>
            </div>
        </div>
    </section>
    <!-- /.card -->
@endsection
