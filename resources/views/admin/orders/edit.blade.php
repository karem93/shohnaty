@extends('admin.layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">الرئيسية - تعديل بيانات الطلب</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">الرئيسية</a></li>
                        <li class="breadcrumb-item active">تعديل بيانات الطلب</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('orders.update', $order->id) }}" method="post"
                          enctype="multipart/form-data" class="mx-auto">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>بيانات الطلب</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td>صاحب الطلب</td>
                                        <td>{{ $order->user->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>اسم الشحنة</td>
                                        <td>{{ $order->package->title }}</td>
                                    </tr>
                                    <tr>
                                        <td>الوزن</td>
                                        <td>{{ $order->package->weight }}</td>
                                    </tr>

                                    <tr>
                                        <td>الموبايل</td>
                                        <td>{{ $order->package->phone }}</td>
                                    </tr>

                                    <tr>
                                        <td>تاريخ الطلب</td>
                                        <td>{{ $order->package->date }}</td>
                                    </tr>

                                    <tr>
                                        <td>حالة الطلب</td>
                                        <td>{{  $order->status=='completed'?'مكتملة'
                                            : ($order->status=='pending'?'قيد الانتظار'
                                            :($order->status=='in_progress'?'جاري التنفيذ':'ملغية')) }}</td>
                                    </tr>

                                    <tr>
                                        <td>السائق</td>
                                        <td>{{ $order->driver->name??'لا يوجد' }}</td>
                                    </tr>
                                    <!-- Add more rows as needed -->
                                    </tbody>
                                    </table>

                                <div class="form-group">
                                    <label for="status">تغيير حالة الطلب</label>
                                    <select name="status" id="" class="form-control">
                                        <option value="pending" @if($order->status=='pending') selected @endif>قيد الانتظار</option>
                                        <option value="in_progress" @if($order->status=='in_progress') selected @endif>جاري التنفيذ</option>
                                        <option value="completed" @if($order->status=='completed') selected @endif>مكتملة</option>
                                        <option value="canceled" @if($order->status=='canceled') selected @endif>ملغية</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="status">تغيير السائق</label>
                                    <select name="driver_id" id="" class="form-control">
                                        @foreach($drivers as $driver)
                                            <option value="{{ $driver->id }}" @if($order->driver_id==$driver->id) selected @endif>
                                                {{ $driver->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <button type="submit" class="btn btn-primary">تعديل</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
