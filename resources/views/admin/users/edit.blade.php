@extends('admin.layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">الرئيسية - تعديل بيانات العميل</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/dashboard">الرئيسية</a></li>
                        <li class="breadcrumb-item active">تعديل بيانات العميل</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form action="{{ route('users.update', $patient->id) }}" method="post"
                          enctype="multipart/form-data" class="mx-auto">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <!-- Name -->
                            <div class="form-group">
                                <label for="exampleInputText1">الاسم</label>
                                <input type="text" class="form-control" name="name" id="exampleInputText1"
                                       placeholder="ادخل الاسم" value="{{ $patient->user->name }}">
                            </div>

                            <!-- Age -->
                            <div class="form-group">
                                <label for="exampleInputText1">العمر</label>
                                <input type="number" class="form-control" name="age" id="exampleInputText1"
                                       placeholder="ادخل العمر" value="{{ $patient->age }}">
                            </div>

                            <!-- Email -->
                            <div class="form-group">
                                <label for="exampleInputEmail1">البريد الإلكتروني</label>
                                <input type="email" class="form-control" name="email" id="exampleInputEmail1"
                                       placeholder="ادخل البريد الإلكتروني" value="{{ $patient->user->email }}">
                            </div>

                            <!-- Password -->
                            <div class="form-group">
                                <label for="exampleInputPassword1">كلمة المرور</label>
                                <input type="password" class="form-control" name="password" id="exampleInputPassword1"
                                       placeholder="ادخل كلمة المرور">
                            </div>

                            <!-- Phone Number -->
                            <div class="form-group">
                                <label for="exampleInputEmail1">رقم الهاتف</label>
                                <input type="text" class="form-control" name="phone" id="exampleInputText1"
                                       placeholder="ادخل رقم الهاتف" value="{{ $patient->user->phone }}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">الصوره</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="exampleInputFile" name="image">
                                        <label class="custom-file-label" for="exampleInputFile"></label>
                                    </div>
                                    <div class="input-group-append">
                                        <br><br>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary">تعديل</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
