<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="copyright" content="MACode ID, https://macodeid.com/">

    <title>شحنتي</title>

    <link rel="stylesheet" href="{{ asset('/assets/css/maicons.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendor/owl-carousel/css/owl.carousel.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/vendor/animate/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/theme.css') }}">
</head>
<body>

<!-- Back to top button -->
<div class="back-to-top"></div>

<header>
    @include('admin.layouts.message')
    <nav class="navbar navbar-expand-lg navbar-light shadow-sm" dir="rtl">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="{{ asset('images/logo.png') }}" alt="logo" srcset="" width="70">
            </a>


            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupport"
                    aria-controls="navbarSupport" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

        </div> <!-- .container -->
    </nav>
</header>

@include('admin.layouts.message')

<section class="vh-100" style="background-color: #508bfc;" dir="rtl">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-12 col-md-8 col-lg-6 col-xl-5">
                <div class="card shadow-2-strong" style="border-radius: 1rem;">
                    <div class="card-body p-5 text-center">

                        <form action="{{ route('doLogin') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <img src="{{ asset('images/logo.png') }}" alt="logo" srcset="" width="70">
                            </div>
                            <div data-mdb-input-init class="form-outline mb-4">
                                <label class="form-label" for="typeEmailX-2">رقم الهاتف</label>
                                <input type="tel" name="phone" id="typeEmailX-2" class="form-control form-control-lg" dir="rtl"/>
                            </div>

                            <div data-mdb-input-init class="form-outline mb-4">
                                <label class="form-label" for="typePasswordX-2">كلمة المرور</label>
                                <input type="password" name="password" id="typePasswordX-2" class="form-control form-control-lg" />
                            </div>

                            <!-- Checkbox -->
                            <div class="form-check d-flex justify-content-start mb-4">
                                <input class="form-check-input" type="checkbox" value="" id="form1Example3" />
                                <label class="form-check-label mr-4" for="form1Example3"> تذكرني</label>
                            </div>

                            <button data-mdb-button-init data-mdb-ripple-init class="btn btn-primary btn-lg btn-block" type="submit">دخول</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<footer class="page-footer">
    <div class="container" style="text-align: center;">
        <p id="copyright text-center">
            جميع الحقوق محفوظة © {{ date('Y') }}
        </p>
    </div>
</footer>

<script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>

<script src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('assets/vendor/owl-carousel/js/owl.carousel.min.js') }}"></script>

<script src="{{ asset('assets/vendor/wow/wow.min.js') }}"></script>

<script src="{{ asset('assets/js/theme.js') }}"></script>

</body>
</html>

