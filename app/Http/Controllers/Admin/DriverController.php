<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DriverCar;
use App\Models\User;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function index()
    {
        $drivers = User::where('user_type','driver')->latest()->get();
        $active = 'drivers';
        return view('admin.drivers.index', compact('drivers', 'active'));
    }

    public function create()
    {
        $active = 'drivers';
        return view('admin.drivers.create', compact('active'));
    }

    public function store(Request $request)
    {
        // Validate the request data
        $request->validate([
            'name' => 'required|string',
            'password' => 'required|string|min:6',
            'phone' => 'required|string|unique:users,phone',
            'identity_number' => 'required|digits:14|unique:users,identity_number',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
            'plate_number' => 'required|string',
            'car_type' => 'required|string',
            'driving_license' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
            'car_insurance' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
            'accessories' => 'array',
            'accessories.*' => 'string',
        ]);
        // Create a new user
        $driver = User::create([
            'name' => $request->input('name'),
            'user_type' => 'driver',
            'password' => bcrypt($request->input('password')),
            'phone' => $request->input('phone'),
            'identity_number' => $request->input('identity_number'),
        ]);

        // Upload and store the image if provided
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $imageName);
            $driver->update([
                'image' => $imageName
            ]);
        }


        // Create and Save the driver car to the database
        $driverCar = DriverCar::create([
            'driver_id' => $driver->id,
            'plate_number' => $request->plate_number,
            'car_type' => $request->car_type,
        ]);

        // Upload and store the driving_license if provided
        if ($request->hasFile('driving_license')) {
            $driving_license = $request->file('driving_license');
            $driving_license_name = \Str::random(5) . '_' . time() . '.' . $driving_license->getClientOriginalExtension();
            $driving_license->move(public_path('uploads'), $driving_license_name);
            $driverCar->driving_license = $driving_license_name;
            $driverCar->save();
        }


        // Upload and store the car_license if provided
        if ($request->hasFile('car_license')) {
            $car_license = $request->file('car_license');
            $car_license_name = \Str::random(5) . '_' . time() . '.' . $car_license->getClientOriginalExtension();
            $car_license->move(public_path('uploads'), $car_license_name);
            $driverCar->car_license = $car_license_name;
            $driverCar->save();
        }
        foreach ($request->accessories as $key => $accessory) {
            $driverCar->accessories()->create([
                'accessory' => $accessory
            ]);
        }

        return redirect()->route('drivers.index')->with('success', 'تم اضافة السائق بنجاح.');
    }


    public function show($id)
    {
        $user = User::findOrFail($id);
        $active = 'drivers';
        return view('admin.drivers.show', compact('user', 'active'));
    }

    public function edit($id)
    {
        $driver = User::findOrFail($id);
        $accessories = $driver->car?->accessories->map(function ($accessory) {
            return $accessory->accessory;
        })->toArray();

        $active = 'drivers';
        return view('admin.drivers.edit', compact('driver','accessories', 'active'));
    }

    public function update(Request $request, $id)
    {
        // Find the user by ID
        $user = User::findOrFail($id);
        // Validate the request data
        $request->validate([
            'name' => 'required',
            'password' => 'nullable|string|min:6',
            'phone' => 'required|string|unique:users,phone,' . $user->id,
            'identity_number' => 'required|digits:14|unique:users,identity_number,' . $user->id,
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
            'plate_number' => 'required|string',
            'car_type' => 'required|string',
            'driving_license' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
            'car_insurance' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
            'accessories' => 'array',
            'accessories.*' => 'string',
        ]);

        $user->update([
            'name' => $request->input('name') ?? $user->name,
            'phone' => $request->input('phone') ?? $user->phone,
            'identity_number' => $request->input('identity_number') ?? $user->identity_number,
            'password' => $request->input('password') ? bcrypt($request->input('password')) : $user->password,
        ]);

        // Upload and store the new image if provided
        if ($request->hasFile('image')) {
            // Delete the old image if exists
            if ($user->image) {
                unlink(public_path('uploads/' . $user->image));
            }
            // Upload and store the new image
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $imageName);
            $user->image = $imageName;
            $user->save();
        }
        // Create and Save the driver car to the database
        $driverCar = $user->car()->updateOrCreate([
            'driver_id'   => $user->id,
        ],[
            'plate_number' => $request->plate_number,
            'car_type' => $request->car_type,
        ]);

        // Upload and store the driving_license if provided
        if ($request->hasFile('driving_license')) {
            $driving_license = $request->file('driving_license');
            $driving_license_name = \Str::random(5) . '_' . time() . '.' . $driving_license->getClientOriginalExtension();
            $driving_license->move(public_path('uploads'), $driving_license_name);
            $driverCar->driving_license = $driving_license_name;
            $driverCar->save();
        }


        // Upload and store the car_license if provided
        if ($request->hasFile('car_license')) {
            $car_license = $request->file('car_license');
            $car_license_name = \Str::random(5) . '_' . time() . '.' . $car_license->getClientOriginalExtension();
            $car_license->move(public_path('uploads'), $car_license_name);
            $driverCar->car_license = $car_license_name;
            $driverCar->save();
        }

        $driverCar->accessories()->delete();

        foreach ($request->accessories as $key => $accessory) {
            $driverCar->accessories()->create([
                'accessory' => $accessory
            ]);
        }

        // Redirect to the index route
        return redirect()->route('drivers.index')->with('success', 'تم تعديل السائق بنجاح');
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        dd($user);
        // Delete the associated image if it exists
        if ($user->image) {
            $imagePath = public_path('uploads/' . $user->image);
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
        $user->car()->delete();
        $user->delete();

        return redirect()->route('drivers.index')->with('success', 'تم مسح السائق بنجاح.');
    }
}
