<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::all();
        $active = 'orders';
        return view('admin.orders.index', compact('orders','active'));
    }

    public function create()
    {
       //
    }

    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $order = Order::findOrFail($id);
        $active = 'orders';
        return view('admin.orders.show', compact('order','active'));
    }

    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $drivers = User::where('user_type','driver')->latest()->get();
        $active = 'orders';
        return view('admin.orders.edit', compact('order','drivers','active'));
    }

    public function update(Request $request, $id)
    {
        // Find the doctor by ID
        $order = Order::findOrFail($id);
        $order->update($request->all());
        // Redirect to the index route
        return redirect()->route('orders.index')->with('success', 'تم تعديل الطلب بنجاح');
    }


    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        $order->delete();

        return redirect()->route('orders.index')->with('success', 'تم مسح الطلب بنجاح.');
    }
}
