<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        $users = User::where('user_type','user')->latest()->get();
        $active = 'users';
        return view('admin.users.index', compact('users', 'active'));
    }

    public function create()
    {
        $active = 'users';
        return view('admin.users.create', compact('active'));
    }

    public function store(Request $request)
    {
        // Validate the request data
        $request->validate([
            'name' => 'required|string',
            'password' => 'required|string|min:6',
            'phone' => 'nullable|string|unique:users,phone',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
        ]);
        // Create a new user
        $user = User::create([
            'name' => $request->input('name'),
            'user_type' => 'user',
            'password' => bcrypt($request->input('password')),
            'phone' => $request->input('phone'),
        ]);

        // Upload and store the image if provided
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $imageName);
            $user->update([
                'image' => $imageName
            ]);
        }

        return redirect()->route('users.index')->with('success', 'تم اضافة العميل بنجاح.');
    }


    public function show($id)
    {
        $user = User::findOrFail($id);
        $active = 'users';
        return view('admin.users.show', compact('user', 'active'));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $active = 'users';
        return view('admin.users.edit', compact('user', 'active'));
    }

    public function update(Request $request, $id)
    {
        // Find the user by ID
        $user = User::findOrFail($id);
        // Validate the request data
        $request->validate([
            'name' => 'required',
            'password' => 'nullable|string|min:6',
            'phone' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:8192',
        ]);

        // Update other fields
        $user->update([
            'age' => $request->input('age'),
        ]);

        // Update the associated user if email or phone is provided
        if ($request->filled('email') || $request->filled('phone')) {
            $user = User::find($user->user_id);
            $user->update([
                'email' => $request->input('email') ?? $user->email,
                'name' => $request->input('name') ?? $user->name,
                'phone' => $request->input('phone') ?? $user->phone,
            ]);
        }

        // Upload and store the new image if provided
        if ($request->hasFile('image')) {
            // Delete the old image if exists
            if ($user->image) {
                unlink(public_path('uploads/' . $user->image));
            }

            // Upload and store the new image
            $image = $request->file('image');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $imageName);
            $user->image = $imageName;
            $user->save();
        }

        // Redirect to the index route
        return redirect()->route('users.index')->with('success', 'تم تعديل العميل بنجاح');
    }


    public function destroy($id)
    {
        $user = User::findOrFail($id);
        // Delete the associated image if it exists
        if ($user->image) {
            $imagePath = public_path('uploads/' . $user->image);
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
        }
        $user->delete();

        return redirect()->route('users.index')->with('success', 'تم مسح العميل بنجاح.');
    }
}
