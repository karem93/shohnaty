<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    // Display a listing of the users
    public function showLoginForm()
    {
        return view('login');
    }

    // Login function
    public function login(Request $request)
    {
        // Validate the request data
        $request->validate([
            'phone' => ['required', 'string', 'max:255'],
            'password' => 'required',
        ]);
        // Attempt to log in the user
        $credentials = $request->only('phone', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed
            return redirect()->intended('/admin/dashboard');
        }
        // Authentication failed
        return back()->withErrors(['email' => 'Invalid credentials'])->withInput($request->only('email'));
    }

    // Log the user out
    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
